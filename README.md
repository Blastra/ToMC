Tools of Mass Creation

NOTE: Currently server functionality achieved only on Linux in Blender version 2.79b.

Run LinuxOpenRequiredPorts.py found in the Extra_python_modules folder to be able to host a game.

You may need to make the server scripts found in the Extra_python_modules executable by doing 'chmod a+x HTTPServerstuff.py'

Tools of Mass Creation is a game on the Blender game engine. Reshape the forms you create. Collaborate with your friends.

If you wish to host a game, navigate to Play -> Host -> Start Game

If you wish to connect to a hosted game, navigate to Play -> Join -> Input the target IP address -> Connect

Firewalls may prevent the UDP messages of clients from getting through to the server.

Create a server through the menu presented at game start.

Save the world by pressing J.

Load the world by pressing L. 

A world with some ready objects should be available for use.

Zoom with mouse wheel, hold down mouse wheel and move the mouse to orbit the camera around the player object.

Select vertices with left mouse button, unselect with right mouse button.

Move selected vertices by holding left Ctrl + mouse movement.

Select/deselect objects with shift + left mouse button.

Functions accessible from the UI from left to right: 

Add object 

Create STL file of selected object in Exported_Objects/STL

Parent selected objects with the player object (Move grabbed objects around by holding alt + mouse movement or using mouse wheel)

Drill an object by intersecting it with a grabbed object (see below)

Open/close a color wheel to change the color of all selected objects

Combine a copy of a grabbed object with an intersecting object (see below)

Undo

Redo

Return player object to its home position

Turn grabbed objects intangible so the Drill and Combine commands above will work

Delete selected objects

Spatial and mesh deformation data synchronization across the network are mostly functional.

Use WASD and the spacebar to move around with your cube. Expect bugs, the complexity of the program is growing.
