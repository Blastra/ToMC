import socket
import sys
from math import ceil as ce
import os
import logging
import zipfile

"""
Sys.argv input values of this script by index:
0: Path to the script itself
1: local active world name
2: requested file type
3: requested object name
4: IP of the client
5: destination port of the file transmission
6: local machine IP

"""

DebugOn = False

#If at any time an error occurs, the errorCascade
#indicates the thread must be shut down gracefully
errorCascade = False

#"""
scriptPath = sys.argv[0]
worldName = sys.argv[1]
fileType = sys.argv[2]
reqObjName = sys.argv[3]
clientIP = sys.argv[4]
destPort = int(sys.argv[5])
#"""

if DebugOn == True:
    debugLogPath = scriptPath.strip(".py")+'log.txt'
    debugText = open(debugLogPath,"w")

def debug(msg, DebugOn):
    if DebugOn == True:
        print(msg)
        debugText.writelines(msg+'\n')


debug("sys.argv[0]: "+str(scriptPath),DebugOn)
debug("sys.argv[1]: "+str(worldName),DebugOn)
debug("sys.argv[2]: "+str(fileType),DebugOn)
debug("sys.argv[3]: "+str(reqObjName),DebugOn)
debug("sys.argv[4]: "+str(clientIP),DebugOn)
debug("sys.argv[5]: "+str(destPort),DebugOn)


#Store the path to the folder which contains the blends of the local world
objLibPath = os.path.join(os.path.split(os.path.split(scriptPath)[0])[0],"ObjectLibrary",worldName)

blendPath = os.path.join(objLibPath,reqObjName+".blend")
picklePath = os.path.join(objLibPath,"GeneratedPickles",reqObjName+'.pickle')
debug("blendPath: "+blendPath,DebugOn)

worldPath = os.path.join(os.path.split(os.path.split(scriptPath)[0])[0],"Worlds",reqObjName)

s = socket.socket()         # Create a socket object
s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR, 1)

sep = '|&|'

#print("clientIP: "+str(clientIP))
#print("destPort: "+str(destPort))

try:
    conn = s.connect((clientIP, destPort))
except ConnectionRefusedError:
    print("Connection refused")
    errorCascade = True
    s.close()
    
    """
    The server will wait for a request packet.
    The request packet contains:

    -Header 'FileReq'
    -File type within ToMC
    -Name of the file
    """
try:
    reqPckg = s.recv(1024).decode('utf-8').split(sep)
    debug("Server received file request package: "+str(reqPckg),DebugOn)
    #print("reqPckg: "+str(reqPckg))
except:
    errorCascade = True
    print("Closed the connection, shutting down BlendTCPFileSending.py")
    s.close()    
    
    
#print("errorCascade: "+str(errorCascade))


while errorCascade == False:
    
    #Parse the requirements of the client

    if reqPckg[0] == 'FileReq':
        debug("File request packet received",DebugOn)
        #fileType = reqPckg[1]
        #fileName = reqPckg[2]
        
        if fileType == 'blend':
            if os.path.exists(blendPath):
                #To avoid files from being opened when incomplete,
                #store the file into a zip archive
                zipPath = blendPath.strip('.blend')+'.zip'
                zipped = zipfile.ZipFile(zipPath,mode='w', compression=zipfile.ZIP_DEFLATED)
                zipped.write(blendPath,os.path.basename(blendPath))
                zipped.close()
                f = open(zipPath,'rb')
            else:
                errorCascade = True
                debug("Requested blend file did not exist: "+blendPath,DebugOn)
                s.close()
                break
        if fileType == 'world':
            if os.path.exists(worldPath):                
                f = open(worldPath,'rb')
            else:
                errorCascade = True
                debug("Requested world file did not exist: "+worldPath,DebugOn)
                s.close()
                break
    else:
        s.close()
        break
    #The file sending works poorly with files of very small size,
    #so the transmission of .bak and .dir files stalls.
    #A solution: All world files are zipped upon creation and
    #the sending works onwards from there
    
    #Store the file contents as 1024-byte packets into a dictionary
    #and close the file
    segDict = {}

    seg = f.read(1024)
    segIndex = 1
    segDict[segIndex] = seg
    #TODO: Have the server keep the stored segments in memory
    #for some time so the file need not be reopened every time
    while seg:                
                
        segIndex += 1
        seg = f.read(1024)
        segDict[segIndex] = seg
        

    lenRead = len(segDict)

    debug("Segment dictionary length: "+str(len(segDict)),DebugOn)

    f.close()

    #A description message is sent after receiving the request.

    #The description packet contains:

    #-Complete file name without the complete path
    #-Number of packets required to make the transaction

    if fileType == 'blend':
        reqObjName += '.zip'

    descrPckt = 'Descr'+sep+reqObjName+sep+str(lenRead)+sep

    #print("descrPckt: "+str(descrPckt))

    while len(descrPckt) < 1024:
        descrPckt+= "t"

    #s.send(bytes(descrPckt,'utf-8'))

    #descrPckt.encode('utf-8')

    s.send(bytes(descrPckt,'utf-8'))
    #s.send(descrPckt)

    debug('Sent description packet: '+descrPckt,DebugOn)

    #l = f.read(1024)
    

    for i in segDict:
        try:
            s.send(segDict[i])
        except:
            print("Ran into EOF exception?")
            s.close()
        
    debug("Done Sending",DebugOn)
    debug("Sent packets: "+str(len(segDict)),DebugOn)
    #conn.close()
    s.close()
    errorCascade = True
        
            
#else:
#    #Close the socket and connection to avoid hanging
#    s.close()
    

