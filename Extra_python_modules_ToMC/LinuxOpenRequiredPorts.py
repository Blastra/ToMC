import os

portListCommon = [8130, 8131, 8136, 8126, 5481, 9006]

portListClient = [9003, 9004, 10001, 11011]

portListServer = [9005]

#Iterate through the required ports for project ToMC
for commonPort in portListCommon:
    os.system('sudo iptables -A INPUT -p udp --dport '+str(commonPort)+' -j ACCEPT')
    os.system('sudo iptables -A OUTPUT -p udp --dport '+str(commonPort)+' -j ACCEPT')

for clientPort in portListClient:
    os.system('sudo iptables -A OUTPUT -p udp --dport '+str(clientPort)+' -j ACCEPT')

for serverPort in portListServer:
    os.system('sudo iptables -A OUTPUT -p udp --dport '+str(serverPort)+' -j ACCEPT')

os.system('sudo iptables -A INPUT -p tcp --dport 11011 -j ACCEPT')
os.system('sudo iptables -A OUTPUT -p tcp --dport 11011 -j ACCEPT')
