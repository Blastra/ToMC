
import socket
import sys
import os
import time
import queue


#This script is started as an external thread by ToMC when hosting
#a game, receives player input through a UDP network socket and stores
#the received commands per player into a list of a maximum length
#within a dictionary per player IP. The ToMC main process will
#request these commands from this thread.

#The message decay time in seconds
#messageDecayTime = 2

#The maximum amount of messages stored for each player IP
#at a given time
#maxMessageCount = 15

#localIP = sys.argv[0]
#localPort = sys.argv[1]

#Establish the parameters of the socket


#Bind the socket to the local IP


#Define the dictionary of received player commands
plrCommands = {}

#Start receiving messages using the socket 


#Separate the origin IP of the incoming message


#If the incoming origin IP is not in the dictionary, add it as a key
#to the with a list with a 2-tuple of the message contents and the timestamp
#it was sent with, if the messageDecayTime or the maximum amount of stored
#messages per IP



#When ToMC main process reads player commands from the list,
#remove the tuples of those commands from the dictionary list in a
#first in-first out order


#Remove a message from the dictionary if its time stamp is further away
#in time than the messageDecayTime or the last message if the
#maximumStoredMessages length has been reached


