import socket
import sys
import os
import time
import signal
import zipfile

"""
File reception script input reference by segments of sys.argv:

0: file path of ToMC reaching this script
1: worldName
2: fileType
3: objectName
4: server IP
5: server port
6: client IP
"""

DebugOn = False

scriptPath = sys.argv[0]
worldName = sys.argv[1]
fileType = sys.argv[2]
fileName = sys.argv[3]
fileSaveName = sys.argv[4]
serverIP = sys.argv[5]
destPort = int(sys.argv[6])
localIP = sys.argv[7]

if DebugOn == True:
    debugLogPath = scriptPath.strip(".py")+'log.txt'
    debugText = open(debugLogPath,"w")

def debug(msg, DebugOn):
    if DebugOn == True:
        print(msg)
        debugText.writelines(msg+'\n')

#try:

debug("Started TCPFileRecv",DebugOn)   

debug("sys.argv[0]: "+str(scriptPath),DebugOn)
debug("sys.argv[1]: "+str(worldName),DebugOn)
debug("sys.argv[2]: "+str(fileType),DebugOn)
debug("sys.argv[3]: "+str(fileName),DebugOn)
debug("sys.argv[4]: "+str(fileSaveName),DebugOn)
debug("sys.argv[5]: "+str(serverIP),DebugOn)
debug("sys.argv[6]: "+str(destPort),DebugOn)
debug("sys.argv[7]: "+str(localIP),DebugOn)

try: 
    #Store the path to the folder which contains the blends of the local world

    if fileType == "blend":
        objLibPath = os.path.join(os.path.split(os.path.split(scriptPath)[0])[0],"ObjectLibrary",worldName)
        if len(fileName) > 4: # and fileName[-6:] != '.blend':
            #Blend files are also transmitted as zip archives
            fileName = fileName.strip('.blend')+'.zip'
        blendPath = os.path.join(objLibPath,fileName)
        picklePath = os.path.join(objLibPath,"GeneratedPickles",fileName+'.pickle')
    if fileType == "world":
        objLibPath = os.path.join(os.path.split(os.path.split(scriptPath)[0])[0],"Worlds")
        #Blend files must keep the same naming convention
        #across the network, but world name overlap is left
        #for the user to manage
        worldPath = os.path.join(objLibPath,fileSaveName+'.zip')

    sending = False

    try:        
        s = socket.socket()
        debug("Socket object created",DebugOn)        
        s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR, 1)
        debug("Socket type and reusability set",DebugOn)        
        s.bind((localIP, destPort))
        debug("Socket binding complete",DebugOn)
        s.listen(10)
        debug("Socket listening initiated",DebugOn)
        sending = True

    except OSError:
        debug("Transport socket already active, interrupting BlendTCPFileRecv.py",DebugOn)
        s.close()

    recvdPackets = 0

    sep = '|&|'

    try:
        while sending == True:            

            chunkGathering = []
            
            debug("TCP RECEIVING SCRIPT IN WHILE LOOP",DebugOn)
            
            c, addr = s.accept()

            """
            The server will wait for a request packet.
            The request packet contains:

            -Header 'FileReq'
            -File type within ToMC
            -Name of the file
            """

            c.send(bytes('FileReq'+sep+fileType+sep+fileName,'utf-8'))
            debug("File request sent to server",DebugOn)
            
            descrPckg = c.recv(1024).decode('utf-8').split(sep)

            debug("Received description packet: "+str(descrPckg),DebugOn)

            msgPrefix = descrPckg[0]
            incFileName = descrPckg[1]
            packetLimit = int(descrPckg[2])

            print("FILE NAME COMPARISON: "+str(incFileName)+" "+str(fileName))
            
            if fileType == "blend":
                

                if incFileName != fileName:
                    debug("File name discrepancy:\nincFileName: "+str(incFileName)+"\nfileName: "+str(fileName),DebugOn)
                    
                    print("SHOULD STOP FILE RECEPTION")
                    sending = False
                    break

                f = open(os.path.join(blendPath),'wb')
                
            if fileType == "world":
                f = open(os.path.join(worldPath),'wb')
            
            dump = c.recv(1024)

            #chunkGathering.append(dump)
            
            f.write(dump)
            recvdPackets += 1        
            
            while dump:
                            
                dump = c.recv(1024)            
                recvdPackets += 1

                #chunkGathering.append(dump)
                
                #time.sleep(0.001)            
                f.write(dump)
                
                debug("recvdPackets: "+str(recvdPackets),DebugOn)
                
                if recvdPackets == packetLimit -1:
                    sending = False
                    debug("Receiving finished",DebugOn)
                    
                    time.sleep(0.001)
                    f.close()
                    c.shutdown(socket.SHUT_RDWR)
                    c.close()                
                    s.close()
                    
                    break

            #Write the received chunks into the file
            """
            for collectedChunk in chunkGathering:
                f.write(collectedChunk)
            
            f.close()
            break
            """
            
    except KeyboardInterrupt:
        debug("Connection interrupted",DebugOn)
        try:        
            c.close()                # Close the connection
            s.close()
            f.close()
        except:
            debug("Skipped a phase in closing files, connections or sockets",DebugOn)

    #When the file transmission is finished, extract the contents of the zip archive
    #into the folder
            
    if fileType == 'world':
        zf = zipfile.ZipFile(worldPath,'r',compression=zipfile.ZIP_DEFLATED)

        zipInfo = zf.infolist()

        for singZip in zipInfo:

            curName = singZip.filename
                        
            curSufx = curName[-4:]


            if curSufx in ['.dat','.bak','.dir']:
            
                fName = singZip.filename.replace(curName,fileSaveName+curSufx)
                #Use the path required by ToMC's file name request
                #extrPath = objLibPath + fileSaveName + zipSuf
                #dbmSavePath = fName.replace(fileName.strip('.zip'),fileSaveName)
                #dbmSavePath = names[ind]
                #dbmSavePath = fileSaveName.strip(

                debug("Creating file "+fName,DebugOn)
                
                f = open(fName, "wb")
                
                f.write(zf.read(singZip))
                f.close()

                moveDest = os.path.join(objLibPath,fName)
                debug("MoveDest: "+moveDest,DebugOn)

                os.rename(fName,moveDest)

            else:
                #Handle all included blend files
                f = open(curName, "wb")
                f.write(zf.read(singZip))
                f.close()

                
                moveDest = os.path.join(os.path.split(os.path.split(scriptPath)[0])[0],"ObjectLibrary",fileSaveName,curName)
                os.rename(curName,moveDest)
            
                
        zf.close()
    
    debug("Finished",DebugOn)

except:
    if DebugOn == True:
        debugText.write("Error")
        debugText.close()
        DebugOn = False

if DebugOn == True:
    debug("File transmission thread successfully finished",DebugOn)
    debugText.close()
