from socket import *
import select
import sys
import os


"""
The file reception script is activated by the BGE right away on
the client side

File reception script input reference by segments of sys.argv:

0: file path of ToMC reaching this script
1: worldName
2: objectName
3: server IP
4: server port
5: client IP


#### STAGES OF TRANSMISSION IN CHRONOLOGICAL ORDER ####

1. Client's BGE starts file reception thread
2. File reception thread sends transmission request to server
3. Server catches the request on the BGE side and starts a file reception thread
4. Server sends description packet of the file to be
   transmitted to the client (default packet length,
   last packet length, amount of packets, transmission ID number)
5. Client adjusts reception parameters to match info sent by
   the server
6. File reception thread starts a while loop that catches incoming
   packets
7. The client loops requesting packets by their segment number
   from the server if their value is False or of the wrong length
8. When all parts of the file have been gathered, the client opens
   the file to the target path, deposits the file segments and closes the file
9. When a sufficient amount of time has passed, the server releases
   the variable which holds the object file
   

"""

print("sys.argv[0]: "+str(sys.argv[0]))
print("sys.argv[1]: "+str(sys.argv[1]))
print("sys.argv[2]: "+str(sys.argv[2]))
print("sys.argv[3]: "+str(sys.argv[3]))
print("sys.argv[4]: "+str(sys.argv[4]))
print("sys.argv[5]: "+str(sys.argv[5]))

#Store the path to the folder which contains the blends of the local world
objLibPath = os.path.join(os.path.split(os.path.split(sys.argv[0])[0])[0],"ObjectLibrary",sys.argv[1])

blendPath = os.path.join(objLibPath,sys.argv[2]+".blend")
picklePath = os.path.join(objLibPath,"GeneratedPickles",sys.argv[2]+'.pickle')

reqObjName = sys.argv[2]
serverIP = sys.argv[3]
destPort = int(sys.argv[4])
localIP = sys.argv[5]

#print('objLibPath: '+str(objLibPath))

#Define the socket which will receive the file segments 
receptionSocket = socket(AF_INET,SOCK_DGRAM)
receptionSocket.setsockopt(SOL_SOCKET,SO_REUSEADDR, 1)

#Define the socket which will send the description and segment
#requests
requestSocket = socket(AF_INET,SOCK_DGRAM)
requestSocket.setsockopt(SOL_SOCKET,SO_REUSEADDR, 1)

#Bind the socket to the 
client_address = (localIP,destPort)
receptionSocket.bind(client_address)
serv_address = (serverIP,destPort)

#Continue looping until all file segments have been 
segmentedFileRec = {}

#Prepare a manual interruption flag
manualInterruption = False


#Send a description packet request to the server until it arrives

print("Description packet requesting started")

descriptionReceived = False
fileComplete = False

#Debug variable to check how many packets have been received
packageCounter = 0
packageCheckPt = 1000

while descriptionReceived == False and packageCounter < 10000:

    #requestSocket.sendto(bytes("Description"+"|&|"+reqObjName,'utf-8'),serv_address)

    #REMOVEME: Just a counter to force a timeout
    packageCounter += 1

    if packageCounter >= packageCheckPt:
        print("Package counter: "+str(packageCounter))
        packageCheckPt += 1000

    #Keep the socket available to traffic
    receptionSocket.setblocking(0)

    #Check if a packet has arrived
    ready = select.select([receptionSocket], [], [], 0.001)
    if ready[0]:
        data = receptionSocket.recvfrom(1024)

        print(data)

        #Split the data along the '|&|' symbol combinations
        chunkedData = data[0].split(bytes("|&|",'utf-8'))

        #If the incoming message starts with Dcl - descriptionlist
        if str(chunkedData[0]) == bytes("b'Dcl",'utf-8'):
            
            #Separate the different data from the description packet
            
            #Store the default segment length
            segLen = int(chunkedData[1])

            #Store the last segment length
            lastSegLen = int(chunkedData[2])

            #Store the amount of segments
            segAmnt = int(chunkedData[3])

            #Store the incoming object name
            incObjName = str(chunkedData[4])

            #If the file name matches that in the file description,
            #confirm the description reception
            print("incObjName: "+str(incObjName))
            print("reqObjName: "+str(reqObjName))
            print("incObjName == reqObjName: "+str(incObjName == reqObjName))
            
            if incObjName == reqObjName:
                descriptionReceived = True

                print("Description packet obtained successfully.")
                print("Incoming object name: "+str(incObjName))
                print("Segment amount: "+str(segAmnt))
                print("Default segment length: "+str(segLen))
                print("Last segment length: "+str(lastSegLen))

        if bytes("b'Seg",'utf-8') and str(chunkedData[1]) == reqObjName:

            segmentIndex = int(chunkedData[2])

            payload = chunkedData[3]
                        
            #Separate the different data from the description packet

            fileDict[segmentIndex] = payLoad
            
            

if descriptionReceived == False:
    print("Object file requesting timed out.")
else:

    #### FILE SEGMENT REQUESTING BEGINS HERE ####
    
    #Generate a dictionary to hold the packets, filling
    #it with false booleans
    fileDict = {}    
    for segNo in range(segAmnt):
        fileDict[segNo+1] = False

        #Request each segment right away
        segReq = 'Req'+sep+reqObjName+sep+str(segNo+1)

    sep = "|&|"
    
    #Start sending specific packet requests
    while lenReqs == False and boolReq == False:

        if fileDict[segAmnt-1] != False:
            if len(fileDict[segAmnt-1]) == lastSegLen:
                for chkLen in range(segAmnt-1):                
                    if len(fileDict[chkLen]) != segLen:
                        segReq = 'Req'+sep+reqObjName+sep+str(chkLen)
                        requestSocket.sendto(segReq,serv_address)
                        break
                    else:
                        lenReqs = True

            
        
        
        

    #### FILE SEGMENT REQUESTING ENDS HERE ####    

#while False in segmentedFileRec.values() or manualInterruption == True:


#Close the socket
receptionSocket.close()
requestSocket.close()
"""

kolmossukka = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
kolmossukka.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR, 1)
kolmossukka.bind((ServerIP, 10001))

byteCopy = open("/home/arad/Desktop/EstanciaUDPCopy.mp3",'wb')

h = 0

fileComplete = False

segmentedFileRec = {}

manualInterruption = False
completed = False

#Artificially prepare a dict of the right size
segmentCount = 9167
#segmentCount = 9168
for segNo in range(segmentCount+1):
    segmentedFileRec[segNo] = False

segMileStone = 1000

while False in segmentedFileRec.values() or manualInterruption == True:
    try:
        kolmossukka = kolmossukka
        kolmossukka.setblocking(0)
        ready = select.select([kolmossukka], [], [], 0.001)
        # TAKE NOTICE: These actions must be generalised and individualized to
        # work on all player objects.

        if ready[0]:    
            data = kolmossukka.recvfrom(1024)
            #print(data[0])
            chunkedData = data[0].split(bytes("|&|","utf-8"))
            
            if int(chunkedData[1]) > segMileStone:
                print(int(chunkedData[1]))
                segMileStone += 1000
                
            if segmentedFileRec[int(chunkedData[1])] == False:
                if len(chunkedData[0]+chunkedData[2]) == 496 or int(chunkedData[1]) == segmentCount:
                    #print("Received new data segment "+str(int(chunkedData[1]))) 
                    segmentedFileRec[int(chunkedData[1])] = chunkedData[0]+chunkedData[2]
            #print(chunkedData[0]+chunkedData[2])
            #print(data)
        h += 1

        if False not in segmentedFileRec.values():
            completed = True
        
    except KeyboardInterrupt:
        print("Interrupted world file reception")
        manualInterruption = True
        for seg in segmentedFileRec:
            byteCopy.write(segmentedFileRec[seg])

        byteCopy.close()
        break

        
if completed == True:
    print("Received all required segments of data")

    for seg in segmentedFileRec:
        byteCopy.write(segmentedFileRec[seg])

    byteCopy.close()

"""

