from math import ceil as ce
from socket import *
import select
import time
import sys
import os

#This script will read a local blend file, segment it
#into bytes and send the file segments to the client
#first without specific requests and after that listening
#to the client's requests for specific packets

"""
File sending script input reference by segments of sys.argv:

0: file path of ToMC reaching this script
1: worldName
2: objectName
3: server IP
4: server port
5: client IP
6: client port


#Flag to denote whether the client has requested a specific packet yet
clientRequested = False

"""
#Store the path to the folder which contains the blends of the local world
objLibPath = os.path.join(os.path.split(os.path.split(sys.argv[0])[0])[0],"ObjectLibrary",sys.argv[1])

blendPath = os.path.join(objLibPath,sys.argv[2]+".blend")
picklePath = os.path.join(objLibPath,"GeneratedPickles",sys.argv[2]+'.pickle')

worldname = sys.argv[1]
reqObjName = sys.argv[2]
serverIP = sys.argv[3]
servPort = int(sys.argv[4])
clientIP = sys.argv[5]
clientPort = int(sys.argv[6])

#Define the segment-sending socket
sendingSocket = socket(AF_INET,SOCK_DGRAM)
sendingSocket.setsockopt(SOL_SOCKET,SO_REUSEADDR, 1)

#Define the request-receiving socket
reqRecvSocket = socket(AF_INET,SOCK_DGRAM)
reqRecvSocket.setsockopt(SOL_SOCKET,SO_REUSEADDR, 1)

#Universal separator for message segments
sep = '|&|'

### SEND DESCRIPTION PACKET TO THE CLIENT ###

#Calculate the length of the entire message

### OPEN THE BLEND FILE, SEGMENT IT AND STORE IT INTO MEMORY###

byteBlend = open(os.path.join(blendPath,worldName,objName),'rb')

blendSegs = {}

#Calculate the amount of segments

#The safest uncorrupted UDP packet length is 508 bytes
UDPfragSafeLen = 508

#Remove symbols equal to the extra symbols required, which are
#Seg (3)
#|&| between each chunk (3*3 = 9)
#Object name ( len(objName) )
#Segment number (varies, better just guesstimate 7 digits, should be enough)

segLen = UDPfragSafeLen - reqObjName - 12 - 7

fileLength = int(byteBlend.read())

segAmnt = ce(len(fileLength)/segLen)

#Find the remainder and calculate the
#length of the last segment
lastSegLen = (fileLength % segLen) * segLen

byteBlend.seek(0)

sendingLibrary = {}

#Iterate through the opened blend file
for seI in range(reqSegs):
    sendingLibrary[seI+1] = byteBlend.read(segLen)

#A message segment holds:
#Segment identifier prefix "Seg"
#Object name   (TODO: have the client and server agree on a short abbreviation)
#Segment index
#Payload

descrPck = "Dcl"+sep+segLen+sep+lastSegLen+sep+segAmnt+sep+reqObjName

#Keep sending the description packet until the client responds with a
#specific packet

try:
    while clientRequested == False:
    
        #Keep a small delay between description packets
        time.sleep(0.01)
        sendingSocket.sendto(bytes(descrPck,'utf-8'),(clientIP,clientPort))
        
except KeyboardInterrupt:
    print("Interrupted")
    sendingSocket.close()

### WAIT FOR CLIENT TO SEND THE FIRST SPECIFIC SEGMENT REQUEST ###

sendingCompleted = False

while sendingCompleted == False:
    ready = select.select([receptionSocket], [], [], 0.00001)
    if ready[0]:
        data = receptionSocket.recvfrom(1024)

        #Split the data along the '|&|' symbol combinations
        chunkedData = data[0].split(bytes("|&|",'utf-8'))

            #Activate a flag for sending mode
            sendingStarted = True

        

        #If the sending is already underway, respond to a specific request
        if str(chunkedData[0]) = bytes("Req",'utf-8') and sendingStarted == True:
            
            #Store the requested object's name from the first request
            sendObjName = str(chunkedData[1])

            sendObjInd = chunkedData[2]

            ### WAIT FOR SPECIFIC SEGMENT REQUESTS AND RESPOND TO THEM ###

            payLoad = sendingLibrary[int(sendObjInd)]

            segMes = 'Seg'+sep+sendObjName+sep+str(sendObjInd)+sep+payLoad

            sendingSocket.sendto(bytes(segMes,'utf-8'),(clientIP,clientPort))

            
            
        
if sendingStarted == True:    

    for seg in range(segAmnt):
        #payLoad = byteDbm.read(segLen)
        
        payLoad = sendingLibrary[seg+1]

        ### LOOP THROUGH FILE SEGMENTS AND SEND THEM TO THE CLIENT ###

        segMes = 'Seg'+sep+reqObjName+sep+str(seg+1)+sep+payLoad

        sendingSocket.sendto(bytes(segMes,'utf-8'),(clientIP,clientPort))

        time.sleep(0.0000001)

    

    

        


    
    

### CLOSE THE SOCKET ###

sendingSocket.close()
reqRecvSocket.close()
print("Finished")


"""
#This script can be paired with ReceiveReconstructFile.py
#to demonstrate sending files

def getExternalIP():
    sock = socket(AF_INET,SOCK_STREAM)
    sock.connect(('google.com', 80))
    ip = sock.getsockname()[0]
    sock.close()
    return ip

ServerIP = getExternalIP()

byteDbm = open("/home/arad/Desktop/Estancia.mp3",'rb')

dbmSegs = {}

print(dir(byteDbm))

reqLen = ce(len(byteDbm.read())/496.0)

print("reqLen: "+str(reqLen))

byteDbm.seek(0)

sendingLibrary = {}

for seg in range(reqLen):
    sendingLibrary[seg] = bytes("|&|"+str(seg)+"|&|","utf-8")+byteDbm.read(496)

while 1:
    time.sleep(0.0000000001)
    for piece in sendingLibrary:
        
        #print(type(seg))

        sendSock = socket( AF_INET, SOCK_DGRAM ) 
        sendSock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

        sendSock.sendto(sendingLibrary[piece],(ServerIP,10001))
        #print(str(sendingLibrary[piece])+str(piece))

byteDbm.close()

"""


